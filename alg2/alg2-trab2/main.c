#include<stdio.h>


//preenche vetor com -1
void inicializa(int *v, int tam);

//resolve o problema
void resolve(int n, int pMax, int *v, int *p, int *solAtual, int idxSol, int *maior);

void leVetores(int *v, int *p, int n);

void printaVetor(int *v);

//retorna a soma dos valores dos itens de vet
int valor(int *vet, int *v);

int main()
{
    int n; //numero de presentes
    int pMax; //capacidade maxima do saco
    scanf("%d", &n);
    scanf("%d", &pMax);
    int v[n]; //valor sentimental
    int p[n]; //peso
    leVetores(v,p,n);

    //dados todos os pesos > 1, a solucao nao tera tamanho maior que pMax
    int maior[pMax];
    int solAtual[pMax];

    inicializa(maior, pMax);
    inicializa(solAtual, pMax);
    resolve(n, pMax, v, p, solAtual, 0, maior);
    printf("solucao: ");
    printaVetor(maior);
    printf("%d\n", valor(maior, v));
}

//preenche vetor com -1
void inicializa(int *v, int tam)
{
    for(int i = 0; i < tam; i++)
        v[i] = -1;
}

//retorna a soma dos pesos dos itens de v
int peso(int *vet, int *p)
{
    int soma = 0;
    for(int i = 0; vet[i] != -1; i++)
    {
        soma = soma + p[vet[i]];
    }
    return soma;
}


//retorna a soma dos valores dos itens de v
int valor(int *vet, int *v)
{
    int soma = 0;
    for(int i = 0; vet[i] != -1; i++)
    {
        soma = soma + v[vet[i]];
    }
    return soma;
}

//so usado para printar o resultado
void printaVetor(int *v)
{
    for (int i = 0; v[i] != -1; i++) printf("%d ", v[i]+1); //+1 para acertar o indice
    printf("\n");
}

//copia v1 para v2
void copia(int *v1, int *v2)
{
    int i;
    for(i = 0; v1[i] != -1; i++)
        v2[i] = v1[i]; //copia valores

    for(int j = i; v2[j] != -1; j++)
        v2[j] = -1; //preenche o resto de v2 com -1
}

void leVetores(int *v, int *p, int n)
{
    for(int i = 0; i < n; i++)
        scanf("%d %d", &v[i], &p[i]);
}

//retorna se i esta em v
int jaUsado(int *v, int i)
{
    for(int j = 0; v[j] != -1; j++)
    {
        if (v[j] == i) return 1;
    }
    return 0;
}

//retorna se um item pode ser posto no saco
int valido(int *solAtual, int *p, int pMax, int i)
{
    return ((peso(solAtual, p) + p[i]) <= pMax) && (!jaUsado(solAtual, i));
}

void resolve(int n, int pMax, int *v, int *p, int *solAtual, int idxSol, int *maior)
{
    for (int i = 0; i < n; i++)
    {
        if (valido(solAtual, p, pMax, i))
        {
            solAtual[idxSol] = i;
            resolve(n, pMax, v, p, solAtual, idxSol + 1, maior);
            solAtual[idxSol] = -1;
        }
        else if (valor(solAtual, v) > valor(maior, v))
            copia(solAtual, maior);
    }
    return;
}

#include <stdlib.h>
#include <time.h>
#include <stdio.h>


//popula vetor com os indices ao contrario
void popula_reverso(int *vetor, int tamVetor)
{
    for (int i = 0; i < tamVetor; i++) vetor[tamVetor - 1 - i] = i;
}

//popula vetor com os indices
void popula_ordenado(int *vetor, int tamVetor)
{
    for (int i = 0; i < tamVetor; i++) vetor[i] = i;
}

//popula vetor aleatoriamente (valores de 0 a range - 1)
void popula_aleatorio(int *vetor, int tamVetor, int range)
{
    srand(time(NULL));
    for (int i = 0; i < tamVetor; i++) vetor[i] = rand() % range;
}

//copia vetor para copiaVetor
void copia_vetor(int *vetor, int *copiaVetor, int tam)
{
    for (int i = 0; i < tam; i++) copiaVetor[i] = vetor[i];
}

void printa_vetor(int *vetor, int tamVetor)
{
    for (int i = 0; i > tamVetor; i++) printf("%d ", vetor[i]);
    printf("\n");
}



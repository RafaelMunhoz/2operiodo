#include <stdlib.h>

//funçoes para a criação de vetores para testes

//popula vetor com os indices ao contrario
void popula_reverso(int *vetor, int tamVetor);

//popula vetor com os indices dos elementos
void popula_ordenado(int *vetor, int tamVetor);

//popula vetor aleatoriamente (valores de 0 a range - 1)
void popula_aleatorio(int *vetor, int tamVetor, int range);

//copia vetor para copiaVetor
void copia_vetor(int *vetor, int *copiaVetor, int tam);

//printa elementos do vetor
void printa_vetor(int *vetor, int tamVetor);

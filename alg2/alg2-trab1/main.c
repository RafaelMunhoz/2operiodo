#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "vetor.h"
#include "ordenacao.h"

int main(){
	char nome[MAX_CHAR_NOME];
	int numComp;

	//cria vetor
	int tamVetor = 10000;
	int* vetor = malloc(tamVetor * sizeof(int));
	if(vetor == NULL){
		printf("Falha fatal. Impossível alocar memoria.");
		return 1;
	}

	//popula vetor para ordenação com valores de 0 a range - 1
	int range = 1000;
	popula_aleatorio(vetor, tamVetor, range);

	/*cria copia do vetor para passar como parametro das funções de ordenação
	uma vez que essas funçoes alteram o vetor*/
	int* copiaVetor = malloc(tamVetor * sizeof(int));
	if(copiaVetor == NULL){
		printf("Falha fatal. Impossível alocar memoria.");
		return 1;
	}
	copia_vetor(vetor, copiaVetor, tamVetor);

	

	getNome(nome);
	printf("Trabalho de %s\n", nome);
	printf("GRR %u\n", getGRR());

	clock_t start, end;
    double total;

	//Insertion sort
	start = clock();//start recebe o "ciclo" corrente
	numComp = insertionSort(copiaVetor, tamVetor);
	end = clock();//end recebe o "ciclo" corrente
	//o tempo total é a diferença dividia pelos ciclos por segundo
	total = ((double)end - start)/CLOCKS_PER_SEC;
	printf("INSERTION SORT: \n\tTempo: %f \n\tNúmero comparações: %d\n\n", total, numComp);

	//Selection sort
	copia_vetor(vetor, copiaVetor, tamVetor);
	start = clock();
	numComp = selectionSort(copiaVetor, tamVetor);
	end = clock();
	total = ((double)end - start)/CLOCKS_PER_SEC;
	printf("SELECTION SORT: \n\tTempo: %f \n\tNúmero comparações: %d\n\n", total, numComp);

	//Merge sort
	copia_vetor(vetor, copiaVetor, tamVetor);
	start = clock();
	numComp = mergeSort(copiaVetor, tamVetor);
	end = clock();
	total = ((double)end - start)/CLOCKS_PER_SEC;
	printf("MERGE SORT: \n\tTempo: %f \n\tNúmero comparações: %d\n\n", total, numComp);

	//Quick sort
	copia_vetor(vetor, copiaVetor, tamVetor);
	start = clock();
	numComp = quickSort(copiaVetor, tamVetor);
	end = clock();
	total = ((double)end - start)/CLOCKS_PER_SEC;
	printf("QUICK SORT: \n\tTempo: %f \n\tNúmero comparações: %d\n\n", total, numComp);

	//Heap sort
	copia_vetor(vetor, copiaVetor, tamVetor);
	start = clock();
	numComp = heapSort(copiaVetor, tamVetor);
	end = clock();
	total = ((double)end - start)/CLOCKS_PER_SEC;
	printf("HEAP SORT: \n\tTempo: %f \n\tNúmero comparações: %d\n\n", total, numComp);

	//refaz vetor para buscas
	tamVetor = 10000;
	vetor = realloc(vetor,tamVetor * sizeof(int));
	if (vetor == NULL)
	{
		printf("Falha fatal. Impossível alocar memoria.");
		return 1;
	}
	popula_ordenado(vetor, tamVetor);
	
	//escolhe um valor aleatorio para ser buscado
	srand(time(NULL));
	int valorBuscado = rand() % tamVetor;
	int idxBusca;

	//busca sequencial
	numComp = 0;
	start = clock();
	idxBusca = buscaSequencial(vetor, tamVetor, valorBuscado, &numComp);
	end = clock();
	total = ((double)end - start)/CLOCKS_PER_SEC;
	printf("BUSCA SEQUENCIAL: \n\tTempo: %f \n\tNúmero comparações: %d\n\tIdx busca: %d\n\n", total, numComp, idxBusca);

	//busca binaria
	numComp = 0;
	start = clock();
	idxBusca = buscaBinaria(vetor, tamVetor, valorBuscado, &numComp);
	end = clock();
	total = ((double)end - start)/CLOCKS_PER_SEC;
	printf("BUSCA BINÁRIA: \n\tTempo: %f \n\tNúmero comparações: %d\n\tIdx busca: %d\n\n", total, numComp, idxBusca);

	//libera memoria alocada
	free(copiaVetor);
	free(vetor);

	return 0;
}

#include "ordenacao.h"
#include <string.h>
#include <stdlib.h>

void getNome(char nome[]){
	//substitua por seu nome
	strncpy(nome, "Rafael Munhoz da Cunha Marques", MAX_CHAR_NOME);
	nome[MAX_CHAR_NOME-1] = '\0';//adicionada terminação manual para caso de overflow
}

//a função a seguir deve retornar o seu número de GRR
unsigned int getGRR(){
	return 20224385;
}


//retorna indice se valor estiver em vetor, -1 caso contrario
int buscaSequencial(int vetor[], int tam, int valor, int* numComparacoes){
	if (tam < 0) return -1;
	*numComparacoes = *numComparacoes + 1;
	if (valor == vetor[tam - 1]) return tam - 1;
	return buscaSequencial(vetor, tam - 1, valor, numComparacoes);
}

//recebe vetor ordenado, retorna indice se valor esta no vetor, -1 caso contrario
int _buscaBinaria(int vetor[], int a, int b, int valor, int* numComparacoes){
	if (a > b) return -1;
	int meio = (a + b) / 2;
	
	*numComparacoes = *numComparacoes + 1;
	if (vetor[meio] == valor) return meio; //achou o valor e retorna o indice

	*numComparacoes = *numComparacoes + 1;
	if (valor < vetor[meio]) //o valor esta para a direita
		return _buscaBinaria(vetor, a, meio - 1, valor, numComparacoes);

	//o valor esta para a esquerda
	return _buscaBinaria(vetor, meio + 1, b, valor, numComparacoes);

}

//wrapper
int buscaBinaria(int vetor[], int tam, int valor, int* numComparacoes){
	return _buscaBinaria(vetor, 0, tam - 1, valor, numComparacoes);	
}

//insere v[b] no "subvetor" ordenado v[a..b-1]
void insere(int* vetor, int a, int b, int *numComp)
{
	int pos;
	int inserido = vetor[b];
	//acha posição a inserir
	for (pos = a; (vetor[pos] < vetor[b]) && (pos <= b); pos++)
		*numComp = *numComp + 1;
	*numComp = *numComp + 1;
	
	//move os elementos 
	for (int i = b; i > pos; i--) vetor[i] = vetor[i - 1];

	//insere na posição
	vetor[pos] = inserido; 
}

//implementação recursiva do insertion sort
void _insertionSort(int *vetor, int a, int b, int *numComp)
{
	if (a >= b) return;
	_insertionSort(vetor, a, b-1, numComp);
	insere(vetor,a,b, numComp);
	return;
}

//wrapper
int insertionSort(int vetor[], int tam){
	int numComp = 0;
	_insertionSort(vetor, 0, tam -1, &numComp);
	return numComp;
}

//troca as posições de v[a] e v[b]
void troca(int *vetor, int a, int b)
{
	int aux = vetor[a];
	vetor[a] = vetor[b];
	vetor[b] = aux;
}

//retona o indice do menor elemento do vetor
int minimo(int *vetor, int a, int b, int *numComp)
{
	int min = a;
	for (int i = a + 1; i <= b; i++)
	{
		*numComp = *numComp + 1;
		if (vetor[i] < vetor[min]) min = i; 
	}
	return min;
}

//implementação rescursiva do selection sort
void _selectionSort(int *vetor, int a, int b, int *numComp)
{
	if (a >= b) return;
	troca(vetor, a, minimo(vetor,a,b, numComp));
	_selectionSort(vetor, a + 1, b, numComp);
}


//wrapper
int selectionSort(int vetor[], int tam){
	int numComp = 0;
	_selectionSort(vetor, 0, tam - 1, &numComp);
	return numComp;
}

//Une dois vetores ordenados
void intercala(int *vetor, int a , int b, int m, int *numComp)
{
	//cira vetor auxiliar
	int tamAux = b - a + 1;
	int *vAux = malloc(tamAux * sizeof(int));		

	//intercala
	int i = a; int j = m + 1; int k = 0;
	while ((i <= m) && (j <= b))
	{
		*numComp = *numComp + 1;
		if (vetor[i] <= vetor[j]) vAux[k++] = vetor[i++];
		else vAux[k++] = vetor[j++];
	}

	//lida com possivel diferença de tamanho entre v[a..m-1] e v[m..b]
	while (i <= m) vAux[k++] = vetor[i++];
	while (j <= b) vAux[k++] = vetor[j++];

	//copia vetor auxiliar para dentro do vetor original
	for (int l = 0; l < tamAux; l++) vetor[a + l] = vAux[l];

	free(vAux);
}


//implementação recursiva do merge sort
void _mergeSort(int *vetor, int a, int b, int *numComp)
{
	if (a >= b) return;
	int m = (a + b) / 2;
	_mergeSort(vetor, a, m  , numComp);
	_mergeSort(vetor, m + 1 , b, numComp);
	intercala(vetor, a , b, m, numComp);
	
}

//wrapper
int mergeSort(int vetor[], int tam){
	int numComp = 0;
	_mergeSort(vetor, 0, tam - 1, &numComp);
	return numComp;
}

//manipula vetor para garantir que todos os elemntos a direta de x sao menores que x, e a esquerda maiores
int particiona(int *vetor, int a, int b, int x, int *numComp)
{
	int m = a - 1;
	
	for (int i = a; i <= b; i++)
	{
		*numComp = *numComp + 1;
		if (vetor[i] <= x) //coloca elemento menores que x no começo do vetor
		{
			m++;
			troca(vetor, m, i);
		}
	}
	return m;
}

//implementação recursiva do quick sort
void _quickSort(int *vetor, int a, int b, int *numComp)
{
	if (a >= b) return;
	int m = particiona(vetor, a, b, vetor[b], numComp);
	_quickSort(vetor, a, m - 1, numComp);
	_quickSort(vetor, m + 1, b, numComp);
}


//wrapper
int quickSort(int vetor[], int tam){
	int numComp = 0;
	_quickSort(vetor, 0, tam - 1, &numComp);
	return numComp;
}

//recebe "arvore" cujas "subarvores" sao heap e transforma em heap
void max_heapify(int *vetor, int i, int n, int *numComp)
{
	int maior;
	int dir; int esq;
	if (i == 0) //lida com o caso 2*0 = 0
	{
		esq = 1;
		dir = 2;
	}
	else
	{
		esq = 2*i;
		dir = (2*i) + 1;
	}

	*numComp = *numComp + 1;
	if ((esq <= n) && (vetor[esq] > vetor[i])) maior = esq;
	else maior = i;
	*numComp = *numComp + 1;
	if ((dir <= n) && (vetor[dir] > vetor[maior])) maior = dir;

	if (maior != i)
	{
		troca(vetor, i, maior);
		max_heapify(vetor, maior, n, numComp);
	}
}

//transforma "arvore" em heap
void build_max_heap(int *vetor, int n, int *numComp)
{
	for (int i = n/2; i >= 0; i--) max_heapify(vetor, i, n, numComp);
}

//implementação recursiva do heap sort
void _heapSort(int *vetor, int n, int *numComp)
{
	if (n < 1) return;
	troca(vetor, 0, n);
	max_heapify(vetor, 0, n-1, numComp);
	_heapSort(vetor, n-1, numComp);

}

//wrapper
int heapSort(int vetor[], int tam){
	int numComp;
	build_max_heap(vetor, tam, &numComp);
	_heapSort(vetor, tam - 1, &numComp);
	return numComp;
}
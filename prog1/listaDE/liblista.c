#include <stdio.h>
#include <stdlib.h>

struct noLista 
{
    int dado;
    struct noLista *prox;
    struct noLista *ant;
};

struct listaDE
{
    struct noLista *cabeca;
    struct noLista *cauda;
};

struct listaDE *cria_lista()
{
    struct listaDE *lista = malloc(sizeof(struct listaDE));
    lista->cabeca = NULL;
    lista->cauda = NULL;
    return lista;
}

int vazia(struct listaDE *lista)
{
    return (lista->cabeca == NULL);
}


struct noLista *cria_no(int dado)
{
    struct noLista *novo = malloc(sizeof(struct noLista));
    novo->dado = dado;
    novo->prox = NULL;
    novo->ant = NULL;
    return novo;
}

void insere_cabeca(struct listaDE *lista, struct noLista *novo)
{
    if (lista->cabeca == NULL)
    {
        lista->cabeca = novo;
        lista->cauda = novo;
        return;
    }
    novo->prox = lista->cabeca;
    lista->cabeca->ant = novo;
    lista->cabeca = novo;
}

void insere_cauda(struct listaDE *lista, struct noLista *novo)
{
    if (lista->cauda == NULL) 
    {
        lista->cauda = novo;
        lista->cabeca = novo;
        return;
    }
    lista->cauda->prox = novo;
    novo->ant = lista->cauda;
    lista->cauda = novo;
}

void insere_ordenado(struct listaDE *lista, struct noLista *novo)
{
    if (vazia(lista)) 
    {
        lista->cabeca = novo;
        lista->cauda = novo;
        return;
    }
    struct noLista *aux = lista->cabeca;
    while((aux != NULL) && (aux->dado <= novo->dado)) //peocura posicao do elemento
    {
        aux = aux->prox;
    }

    //insere elemento;
    novo->ant = aux->ant;
    novo->prox = aux;
    if(aux != NULL) aux->ant->prox = novo;
    aux->ant = novo;
}

struct noLista *remove_cabeca(struct listaDE *lista)
{
    struct noLista *cabeca = lista->cabeca;
    lista->cabeca = cabeca->prox;
    lista->cabeca->ant = NULL;
    cabeca->prox = NULL;
    return cabeca;
}

struct noLista *remove_cauda(struct listaDE *lista)
{
    struct noLista *cauda = lista->cauda;
    lista->cauda = cauda->ant;
    lista->cauda->prox = NULL;
    cauda->ant = NULL;
    return cauda;
}

struct noLista *busca_remove(struct listaDE *lista, int dado)
{
    struct noLista *aux = lista->cabeca;
    while((aux->dado != dado) && (aux != NULL)) aux = aux->prox; //busca elemento
    if (aux == NULL) return aux; //retorna NULL se elemento nao estiver na lista
    //remove elemento
    aux->ant->prox = aux->prox;
    aux->prox->ant = aux->ant;
    aux->ant = NULL;
    aux->prox = NULL;
    return aux;
}

void printa_lista(struct listaDE *lista)
{
    struct noLista *aux = lista->cabeca;
    while(aux != NULL) //printa de frente pra tras
    {
        printf("%d ", aux->dado);
        aux = aux->prox;
    }
    printf("\n");
    aux = lista->cauda;
    while(aux != NULL) //printa de tras pra frente
    {
        printf("%d ", aux->dado);
        aux = aux->ant;
    }
    printf("\n");
}

void destroi_lista(struct listaDE *lista)
{
    struct noLista *aux = lista->cauda;
    while (aux->ant != NULL) //libera elementos ate chegar na cabeca
    {
        aux = aux->ant;
        free(aux->prox);
    }
    free(aux); //libera cabeca
    free(lista); //libera lista
}
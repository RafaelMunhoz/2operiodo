#include <stdlib.h>

struct listaDE *cria_lista();

struct noLista *cria_no(int dado);

void insere_cabeca(struct listaDE *lista, struct noLista *novo);

int vazia(struct listaDE *lista);

void insere_cauda(struct listaDE *lista, struct noLista *novo);

void insere_ordenado(struct listaDE *lista, struct noLista *novo);

struct noLista *remove_cabeca(struct listaDE *lista);

struct noLista *remove_cauda(struct listaDE *lista);

struct noLista *busca_remove(struct listaDE *lista, int dado);

void printa_lista(struct listaDE *lista);

void destroi_lista(struct listaDE *lista);


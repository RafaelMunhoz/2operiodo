#include <stdio.h>
#include "liblista.h"

int main()
{
    struct listaDE *lista = cria_lista();
    int op;
    int dado;
    struct noLista *novo;
    struct noLista *removido;

    while(1)
    {
        printf("1 para inserir na cabeca\n2 para inserir na cauda\n3 para inserir ordenado\n4 para remover da cabeca\n5 para remover da cauda\n6 para busca e remove\n7 para printar: \n");
        
        scanf("%d", &op);
        system("clear");
        
        if (op > 7) break;

        if (op == 1)
        {
            printf("digite o dado: ");
            scanf("%d", &dado);
            novo = cria_no(dado);
            insere_cabeca(lista, novo);
        }

        if (op == 2)
        {
            printf("digite o dado: ");
            scanf("%d", &dado);
            novo = cria_no(dado);
            insere_cauda(lista, novo);
        }

        if (op == 3)
        {
            printf("digite o dado: ");
            scanf("%d", &dado);
            novo = cria_no(dado);
            insere_ordenado(lista, novo);
        }

        if (op == 4)
        {
            removido = remove_cabeca(lista);
            free(removido);
        }

        if (op == 5)
        {
            removido = remove_cauda(lista);
            free(removido);
        }

        if (op == 6)
        {
            printf("digite dado a remover: ");
            scanf("%d", &dado);
            removido = busca_remove(lista, dado);
            free(removido);
        }

        if (op == 7)
        {
            printa_lista(lista);
        }
    }
    destroi_lista(lista);
    return 0;
}
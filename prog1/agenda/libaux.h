#include <stdlib.h>
#include <time.h>
#define TAM 24

struct dia
{
    struct tm data;
    char *compromissos[TAM];
    struct dia *prox;
};


//Se dia esta na agenda, terona endereço, retorna null se nao estiver 
struct dia *existe(int dia, int mes, struct dia *agenda);

//printa os compromissos de um dia
void printa_compromissos(struct dia *day);

// retorn 1 se day1/mon1 é depois de day2/mon2
int eh_depois(int day1, int mon1, int day2, int mon2);


//insere dia de maneira ordenada na agenda
struct dia *insere_dia(int dia, int mes, struct dia *agenda);


//le hora e compromisso e insere na agenda, retorna false se horario estiver ocupado
int insere_compromisso(struct dia *day);
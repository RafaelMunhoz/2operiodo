#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#define TAM 24

struct dia
{
    struct tm data;
    char *compromissos[TAM];
    struct dia *prox;
};



//Se dia esta na agenda, retorna endereço, retorna null se nao estiver 
struct dia *existe(int dia, int mes, struct dia *agenda)
{
    struct dia *day = agenda;
    while (day != NULL)
    {
        if ((day->data.tm_mday == dia) && (day->data.tm_mon == mes)) return day;
        day = day->prox;
    }
    return NULL;
}

//printa compromissos de um dia
void printa_compromissos(struct dia *day)
{
    for (int i = 0; i < 24; i++)
    {
        if (day->compromissos[i] != NULL)
        {
            printf("\t%dhrs: %s\n", i , day->compromissos[i]);
        }
    }
    printf("---------\n");
}

// retorna se day1/mon1 é depois de day2/mon2
int eh_depois(int day1, int mon1, int day2, int mon2)
{
    if (mon1 > mon2) return 1;
    if (mon1 < mon2) return 0;
    if (day1 > day2) return 1;
    return 0;
}

 //insere dia de maneira ordenada na agenda
struct dia *insere_dia(int dia, int mes, struct dia *agenda)
{
    struct dia *day = agenda;
    //procura dia imediatamente anterior ao novo dia na agenda
    while ((day->prox != NULL) && (eh_depois(dia,mes,day->prox->data.tm_mday, day->prox->data.tm_mon)))
        day = day->prox;

    //cria novo dia
    struct dia *novo_dia = malloc(sizeof(struct dia));
    for (int i = 0 ; i < TAM ; i++) novo_dia->compromissos[i] = NULL;
    novo_dia->data.tm_mday = dia;
    novo_dia->data.tm_mon = mes;

    //insere novo dia apos seu dia anterior
    novo_dia->prox = day->prox;
    day->prox = novo_dia;

    return novo_dia;
}


//le hora e compromisso e poe no array do dia, retorna disponibilidade do horario
int insere_compromisso(struct dia *day)
{
    int hora;
    //le a hora
    do
    {
        printf("Digite o horário do seu compromisso (hh): ");
        scanf("%d", &hora);
    } while(hora > 23);

    //ve se hora esta vaga
    if (day->compromissos[hora] != NULL) return 0;

    //le o compromisso
    size_t tamstr, tam;
    tam = 1;
    day->compromissos[hora] = malloc(tam * sizeof(char));
    printf("Digite o seu compromisso: ");
    getchar();
    tamstr = getline(&day->compromissos[hora], &tam, stdin);
    day->compromissos[hora][strcspn(day->compromissos[hora], "\n")] = '\0';
    return 1;
}
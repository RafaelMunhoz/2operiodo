#include "libagenda.h"
#include <stdio.h>

int main()
{
    struct dia *agenda; //ponteiro para o primeiro dia da agenda (Dia atual do sistema)
    agenda = inicializa_agenda();
    int op;

    while(1)
    {
        printf("\nDigite uma opção: \n 1-Adicionar compromisso\n 2-Listar compromissos\n 3-Sair\n\n");
        scanf("%d", &op);
        system("clear");

        if (op == 1)
        {
            if (!adiciona_compromisso(agenda))
            {
                system("clear");
                printf("A hora do seu compromisso ja esta ocupada.\n");
            }
            else
            {
                system("clear");
                printf("\nCompromisso marcado na agenda!\n");
            }
        }

        if (op == 2) 
        {
            system("clear");
            lista_compromissos(agenda);
        }   

        if (op == 3) break;

    }

    destroi_agenda(agenda);
    return 0;
}
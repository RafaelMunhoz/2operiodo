#include <stdlib.h>
#include <time.h>
#define TAM 24
/*
- Biblioteca de manipulação de uma agenda
- Os dias sao structs que contem a sua data, um array de compromissos
e um ponteiro para o proximo dia.
- Os dias estao ordenados na lista
*/

//Cria o dia atual, que sera a cabeça da lista de dias
struct dia *inicializa_agenda();

//Le um compromisso e adiciona na agenda
int adiciona_compromisso(struct dia *agenda);

//Lista todos os compromisso da agenda
void lista_compromissos(struct dia *agenda);

//Libera todo o espaço alocado pra agenda
void destroi_agenda(struct dia *agenda);

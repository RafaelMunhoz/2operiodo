#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "libaux.h"
#define TAM 24



//cria dia 1 (atual)
struct dia *inicializa_agenda()
{
    struct dia *dia1 = malloc(sizeof(struct dia));
    time_t data;
    time(&data); //atribui á variavel data a hora do sistema
    dia1->data = *gmtime(&data); //transforma data do sistema em uma struct e atribui ao campo data
    dia1->data.tm_mon++; //na time.h os meses sao indexados 0-11, mas vamos trabalhar com 1-12
    dia1->prox = NULL;
    for (int i = 0; i < TAM; i++) dia1->compromissos[i] = NULL;
    return dia1;
}



//printa os compromissos da agenda
void lista_compromissos(struct dia *agenda)
{
    struct dia *day = agenda;
    while (day != NULL)
    {
        printf("%d/%d\n", day->data.tm_mday, day->data.tm_mon); //printa data
        printa_compromissos(day); //printa compromissos
        day = day->prox;
    }
    return;
}



//le data e hora e insere compromisso na agenda
int adiciona_compromisso(struct dia *agenda)
{
    int diam, mes;
    //le a data
    do
    {
        printf("Digite a data do seu compromisso (dd mm): ");
        scanf("%d %d", &diam, &mes);
    } while ((mes > 12) || (diam > 31));

    struct dia *day = existe(diam, mes, agenda);//acha endereço do dia do compromisso
    if (day == NULL)//se o dia nao esta na agena, insere
    {
        day = insere_dia(diam, mes, agenda); //insere ordenado na agenda e retorna ponteiro
    }

    return insere_compromisso(day); //le hora e compromisso e poe no array do dia, retorna disponibilidade do horario
    
}


//libera memoria alocada para a agenda
void destroi_agenda(struct dia *agenda)
{
    while (agenda->prox != NULL)
    {
        struct dia *destruido = agenda->prox;
        agenda->prox = destruido->prox;

        for (int i = 0; i<24; i++) free(destruido->compromissos[i]);
        free(destruido);
    }
    for (int i = 0; i<24; i++) free(agenda->compromissos[i]);
    free(agenda);
}






